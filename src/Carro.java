
public class Carro {
	private int AnoAquisicao;
	private int AnoVenda;
	private int Cilindrada;
	private String Combustivel;
	private String Cor;
	private String Marca;
	private String Matricula;
	private String Modelo;
	private double PrecoAquisicao;
	private double ValorVenda;
	/**
	 * @param anoAquisicao
	 * @param anoVenda
	 * @param cilindrada
	 * @param combustivel
	 * @param cor
	 * @param marca
	 * @param matricula
	 * @param modelo
	 * @param precoAquisicao
	 * @param valorVenda
	 */
	public Carro(int anoAquisicao, int anoVenda, int cilindrada, String combustivel, String cor, String marca,
			String matricula, String modelo, double precoAquisicao, double valorVenda) {
		super();
		AnoAquisicao = anoAquisicao;
		AnoVenda = anoVenda;
		Cilindrada = cilindrada;
		Combustivel = combustivel;
		Cor = cor;
		Marca = marca;
		Matricula = matricula;
		Modelo = modelo;
		PrecoAquisicao = precoAquisicao;
		ValorVenda = valorVenda;
	}
	/**
	 * @return the anoAquisicao
	 */
	public int getAnoAquisicao() {
		return AnoAquisicao;
	}
	/**
	 * @param anoAquisicao the anoAquisicao to set
	 */
	public void setAnoAquisicao(int anoAquisicao) {
		AnoAquisicao = anoAquisicao;
	}
	/**
	 * @return the anoVenda
	 */
	public int getAnoVenda() {
		return AnoVenda;
	}
	/**
	 * @param anoVenda the anoVenda to set
	 */
	public void setAnoVenda(int anoVenda) {
		AnoVenda = anoVenda;
	}
	/**
	 * @return the cilindrada
	 */
	public int getCilindrada() {
		return Cilindrada;
	}
	/**
	 * @param cilindrada the cilindrada to set
	 */
	public void setCilindrada(int cilindrada) {
		Cilindrada = cilindrada;
	}
	/**
	 * @return the combustivel
	 */
	public String getCombustivel() {
		return Combustivel;
	}
	/**
	 * @param combustivel the combustivel to set
	 */
	public void setCombustivel(String combustivel) {
		Combustivel = combustivel;
	}
	/**
	 * @return the cor
	 */
	public String getCor() {
		return Cor;
	}
	/**
	 * @param cor the cor to set
	 */
	public void setCor(String cor) {
		Cor = cor;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return Marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		Marca = marca;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return Matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return Modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		Modelo = modelo;
	}
	/**
	 * @return the precoAquisicao
	 */
	public double getPrecoAquisicao() {
		return PrecoAquisicao;
	}
	/**
	 * @param precoAquisicao the precoAquisicao to set
	 */
	public void setPrecoAquisicao(double precoAquisicao) {
		PrecoAquisicao = precoAquisicao;
	}
	/**
	 * @return the valorVenda
	 */
	public double getValorVenda() {
		return ValorVenda;
	}
	/**
	 * @param valorVenda the valorVenda to set
	 */
	public void setValorVenda(double valorVenda) {
		ValorVenda = valorVenda;
	}
	
	
	

}
