
public class Combustiveis {
	private String Combustivel;


	/**
	 * @param combustivel
	 */
	public Combustiveis(String combustivel) {
		super();
		Combustivel = combustivel;
	}


	/**
	 * @return the combustivel
	 */
	public String getCombustivel() {
		return Combustivel;
	}


	/**
	 * @param combustivel the combustivel to set
	 */
	public void setCombustivel(String combustivel) {
		Combustivel = combustivel;
	}
	public String toString() {
		  return Combustivel;
		 }


}
