
public class IUC {
	private String DataValidade;
	private int IdIuc;
	private String Matricula;
	private double Valor;
	/**
	 * @param dataValidade
	 * @param idIuc
	 * @param matricula
	 * @param valor
	 */
	public IUC(String dataValidade, int idIuc, String matricula, double valor) {
		super();
		DataValidade = dataValidade;
		IdIuc = idIuc;
		Matricula = matricula;
		Valor = valor;
	}
	/**
	 * @return the dataValidade
	 */
	public String getDataValidade() {
		return DataValidade;
	}
	/**
	 * @param dataValidade the dataValidade to set
	 */
	public void setDataValidade(String dataValidade) {
		DataValidade = dataValidade;
	}
	/**
	 * @return the idIuc
	 */
	public int getIdIuc() {
		return IdIuc;
	}
	/**
	 * @param idIuc the idIuc to set
	 */
	public void setIdIuc(int idIuc) {
		IdIuc = idIuc;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return Matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return Valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		Valor = valor;
	}
	
	
	

}
