import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import com.sun.javafx.tk.Toolkit;
import com.sun.prism.Image;
import jdk.nashorn.internal.ir.RuntimeNode.Request;
import net.proteanit.sql.DbUtils;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
public class projeto {

	private JFrame frmQwerty;
	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:projeto.db";

	Connection conn = null;
	Statement stmt = null;

	private JTextField txt_user;
	private JPasswordField passwordField;
	private JButton btn_login;
	private JButton btn_sair;
	private ButtonGroup bg = new ButtonGroup();
	private ButtonGroup bg_alt_insp = new ButtonGroup();
	private JTable table_dados;
	private JTextField txt_marca;
	private JTextField txt_modelo;
	private JTextField txt_cor;
	private JFormattedTextField txt_cilindrada;
	private JFormattedTextField txt_matricula;
	private JFormattedTextField txt_ano_aqui;
	private JFormattedTextField txt_preco_aqui;
	private JFormattedTextField txt_ano_venda;
	private JFormattedTextField txt_valor_venda;
	private JFormattedTextField txt_data_insp;
	private JFormattedTextField txt_valor_insp;
	private JFormattedTextField txt_valor_iuc;
	private JFormattedTextField txt_validade_iuc;
	private JTextField txt_oficina_rev;
	private JFormattedTextField txt_kms_rev;
	private JFormattedTextField txt_data_rev;
	private JFormattedTextField txt_valor_rev;
	private JFormattedTextField txt_valor_seg;
	private JFormattedTextField txt_validade_seg;
	private JTextField txt_nome_seg;
	private JTable table_carros_anonimo;
	private JTextField txt_novo_uti;
	private JTextField txt_novo_pass;
	private JTextField txt_conf_pass;
	private JTextField txt_alt_marca;
	private JTextField txt_alt_modelo;
	private JTextField txt_alt_cor;
	private JTextField txt_alt_oficina_rev;
	private JTextField txt_alt_nome_seg;
	private JTextField txt_eliminar_marca;
	private JTextField txt_eliminar_modelo;
	private JTextField txt_eliminar_cor;
	
	public static void adicionarMask(String formato, JFormattedTextField field) {
		try {
			MaskFormatter mask = new MaskFormatter(formato);
			mask.install(field);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					projeto window = new projeto();
					window.frmQwerty.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public projeto() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try{
			Class.forName(JDBC_DRIVER);
			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (Exception fe) {
			// Handle errors for Class.forName
			fe.printStackTrace();
		}
		

		frmQwerty = new JFrame();
		frmQwerty.setTitle("Base de Dados Autom\u00F3vel");
		frmQwerty.setBounds(100, 100, 629, 445);
		frmQwerty.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmQwerty.getContentPane().setLayout(new CardLayout(0, 0));


		JPanel login = new JPanel();
		frmQwerty.getContentPane().add(login, "name_29583963884922");
		login.setLayout(null);

		JLabel lblLogin = new JLabel("LOGIN");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblLogin.setBounds(272, 11, 73, 50);
		login.add(lblLogin);

		txt_user = new JTextField();
		txt_user.setBounds(397, 133, 96, 20);
		login.add(txt_user);
		txt_user.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(397, 164, 96, 20);
		login.add(passwordField);

		JLabel lblUtilizador = new JLabel("Utilizador:");
		lblUtilizador.setBounds(313, 136, 74, 14);
		login.add(lblUtilizador);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(313, 167, 74, 14);
		login.add(lblPassword);

		JPanel menu = new JPanel();
		frmQwerty.getContentPane().add(menu, "name_29586206975740");

		JPanel anonimo = new JPanel();
		frmQwerty.getContentPane().add(anonimo, "name_9681681564465");
		anonimo.setLayout(null);

		JButton button = new JButton("Sair");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				anonimo.setVisible(false);
				login.setVisible(true);
			}
		});
		button.setBounds(539, 371, 64, 23);
		anonimo.add(button);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 45, 593, 312);
		anonimo.add(scrollPane_1);

		table_carros_anonimo = new JTable();
		scrollPane_1.setViewportView(table_carros_anonimo);

		JLabel lblListaDeCarros = new JLabel("Lista de Carros");
		lblListaDeCarros.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblListaDeCarros.setBounds(246, 11, 132, 23);
		anonimo.add(lblListaDeCarros);

		JPanel registar = new JPanel();
		frmQwerty.getContentPane().add(registar, "name_12528571580788");
		registar.setLayout(null);
		
		

		txt_novo_uti = new JTextField();
		txt_novo_uti.setBounds(262, 113, 124, 20);
		registar.add(txt_novo_uti);
		txt_novo_uti.setColumns(10);

		txt_novo_pass = new JTextField();
		txt_novo_pass.setColumns(10);
		txt_novo_pass.setBounds(262, 144, 124, 20);
		registar.add(txt_novo_pass);

		JLabel lblRegistarUtilizador = new JLabel("Registar Utilizador");
		lblRegistarUtilizador.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRegistarUtilizador.setBounds(249, 23, 155, 28);
		registar.add(lblRegistarUtilizador);

		txt_conf_pass = new JTextField();
		txt_conf_pass.setColumns(10);
		txt_conf_pass.setBounds(262, 175, 124, 20);
		registar.add(txt_conf_pass);

		JLabel lblNovoUtilizador = new JLabel("Novo Utilizador:");
		lblNovoUtilizador.setBounds(157, 116, 95, 14);
		registar.add(lblNovoUtilizador);

		JLabel lblNovoPassword = new JLabel("Nova Password:");
		lblNovoPassword.setBounds(157, 147, 95, 14);
		registar.add(lblNovoPassword);

		JLabel lblConfirmarPassword = new JLabel("Confirmar Password:");
		lblConfirmarPassword.setBounds(133, 178, 132, 14);
		registar.add(lblConfirmarPassword);

		JButton btn_registar = new JButton("Registar");
		btn_registar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					stmt = conn.createStatement();
					String query2 = "SELECT User FROM Users WHERE User LIKE '" + txt_novo_uti.getText() + "'";
					ResultSet rs = stmt.executeQuery(query2);

					if (rs.isBeforeFirst())
						JOptionPane.showMessageDialog(null, "Utilizador está a ser usado", "Utilizador inválido", JOptionPane.INFORMATION_MESSAGE);
					else
					{
						stmt = conn.createStatement();
						String query = "INSERT INTO Users (User, Pass) VALUES ('"+ txt_novo_uti.getText() +
								"' , '" + txt_novo_pass.getText() + "')";



						if(txt_novo_pass.getText().equals(txt_conf_pass.getText()) && 
								!txt_novo_pass.getText().isEmpty() && 
								!txt_novo_uti.getText().isEmpty() && 
								!txt_conf_pass.getText().isEmpty())
						{
							stmt.executeUpdate(query);
							txt_novo_uti.setText("");
							txt_novo_pass.setText("");
							txt_conf_pass.setText("");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}
					}

				} catch (Exception fe) {
					
					fe.printStackTrace();
				}
			}
		});
		btn_registar.setBounds(279, 219, 89, 23);
		registar.add(btn_registar);
		
		JButton btn_sair_registo = new JButton("Sair");
		btn_sair_registo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txt_novo_uti.setText("");
				txt_novo_pass.setText("");
				txt_conf_pass.setText("");
				registar.setVisible(false);
				login.setVisible(true);
			}
		});
		btn_sair_registo.setBounds(514, 373, 89, 23);
		registar.add(btn_sair_registo);

		JPanel consultar = new JPanel();
		frmQwerty.getContentPane().add(consultar, "name_29588159158564");
		
		JPanel eliminar_carro = new JPanel();
		frmQwerty.getContentPane().add(eliminar_carro, "name_13831970156813");
		eliminar_carro.setLayout(null);
		
		txt_eliminar_marca = new JTextField();
		txt_eliminar_marca.setColumns(10);
		txt_eliminar_marca.setBounds(260, 81, 149, 20);
		eliminar_carro.add(txt_eliminar_marca);
		txt_eliminar_marca.setEditable(false);
		
		txt_eliminar_modelo = new JTextField();
		txt_eliminar_modelo.setColumns(10);
		txt_eliminar_modelo.setBounds(260, 106, 149, 20);
		eliminar_carro.add(txt_eliminar_modelo);
		txt_eliminar_modelo.setEditable(false);
		
		txt_eliminar_cor = new JTextField();
		txt_eliminar_cor.setColumns(10);
		txt_eliminar_cor.setBounds(260, 131, 149, 20);
		eliminar_carro.add(txt_eliminar_cor);
		txt_eliminar_cor.setEditable(false);
		
		JFormattedTextField txt_eliminar_cilindrada = new JFormattedTextField();
		txt_eliminar_cilindrada.setColumns(10);
		txt_eliminar_cilindrada.setBounds(260, 156, 149, 20);
		eliminar_carro.add(txt_eliminar_cilindrada);
		txt_eliminar_cilindrada.setEditable(false);
		
		JFormattedTextField txt_eliminar_ano_aqui = new JFormattedTextField();
		txt_eliminar_ano_aqui.setColumns(10);
		txt_eliminar_ano_aqui.setBounds(260, 235, 149, 20);
		eliminar_carro.add(txt_eliminar_ano_aqui);
		txt_eliminar_ano_aqui.setEditable(false);
		
		JFormattedTextField txt_eliminar_preco_aqui = new JFormattedTextField();
		txt_eliminar_preco_aqui.setColumns(10);
		txt_eliminar_preco_aqui.setBounds(260, 260, 149, 20);
		eliminar_carro.add(txt_eliminar_preco_aqui);
		txt_eliminar_preco_aqui.setEditable(false);
		
		JFormattedTextField txt_eliminar_ano_venda = new JFormattedTextField();
		txt_eliminar_ano_venda.setColumns(10);
		txt_eliminar_ano_venda.setBounds(260, 285, 149, 20);
		eliminar_carro.add(txt_eliminar_ano_venda);
		txt_eliminar_ano_venda.setEditable(false);
		
		JFormattedTextField txt_eliminar_valor_venda = new JFormattedTextField();
		txt_eliminar_valor_venda.setColumns(10);
		txt_eliminar_valor_venda.setBounds(260, 310, 149, 20);
		eliminar_carro.add(txt_eliminar_valor_venda);
		txt_eliminar_valor_venda.setEditable(false);
		
		JFormattedTextField txt_eliminar_combustivel = new JFormattedTextField();
		txt_eliminar_combustivel.setColumns(10);
		txt_eliminar_combustivel.setBounds(260, 210, 149, 20);
		eliminar_carro.add(txt_eliminar_combustivel);
		txt_eliminar_combustivel.setEditable(false);
		
		JComboBox combo_eliminar_matr = new JComboBox();
		
		JButton btn_eliminar = new JButton("Eliminar");
		btn_eliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try{
					stmt = conn.createStatement();
					String Query="DELETE FROM Carro WHERE Matricula LIKE '"+combo_eliminar_matr.getSelectedItem()+"'";
					String Query2="DELETE FROM Inspecao WHERE Matricula LIKE '"+combo_eliminar_matr.getSelectedItem()+"'";
					String Query3="DELETE FROM IUC WHERE Matricula LIKE '"+combo_eliminar_matr.getSelectedItem()+"'";
					String Query4="DELETE FROM Revisao WHERE Matricula LIKE '"+combo_eliminar_matr.getSelectedItem()+"'";
					String Query5="DELETE FROM Seguro WHERE Matricula LIKE '"+combo_eliminar_matr.getSelectedItem()+"'";
					
					int opcao =JOptionPane.showOptionDialog(frmQwerty, "Tem a certeza que pretende eliminar este carro?", "Eliminar carro", JOptionPane.OK_CANCEL_OPTION, 0, null, null, null);
					if(opcao == JOptionPane.OK_OPTION)
					{
						stmt.executeUpdate(Query);
						stmt.executeUpdate(Query2);
						stmt.executeUpdate(Query3);
						stmt.executeUpdate(Query4);
						stmt.executeUpdate(Query5);
						combo_eliminar_matr.removeAllItems();
						if(combo_eliminar_matr.getItemCount()==0)
						{
							stmt = conn.createStatement();
							ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
							while(rs.next())
							{
								Matricula matr = new Matricula(rs.getString("Matricula"));
								combo_eliminar_matr.addItem(matr);
							}
						}
					}
				}catch(Exception fe){
					fe.printStackTrace();
				}
			}
		});
		btn_eliminar.setBounds(292, 353, 89, 23);
		eliminar_carro.add(btn_eliminar);
		
		
		combo_eliminar_matr.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM Carro WHERE Matricula='"+combo_eliminar_matr.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     while (rs.next())
				     {
				    	 txt_eliminar_marca.setText(rs.getString("Marca"));
				    	 txt_eliminar_modelo.setText(rs.getString("Modelo"));
				    	 txt_eliminar_cor.setText(rs.getString("Cor"));
				    	 txt_eliminar_cilindrada.setText(rs.getString("Cilindrada"));
				    	 txt_eliminar_ano_aqui.setText(rs.getString("AnoAquisicao"));
				    	 txt_eliminar_preco_aqui.setText(rs.getString("PrecoAquisicao"));
				    	 txt_eliminar_ano_venda.setText(rs.getString("AnoVenda"));
				    	 txt_eliminar_valor_venda.setText(rs.getString("ValorVenda"));
				    	 txt_eliminar_combustivel.setText(rs.getString("Combustivel"));
				     }
						

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				
			}
		});
		combo_eliminar_matr.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		combo_eliminar_matr.setBounds(260, 185, 149, 20);
		eliminar_carro.add(combo_eliminar_matr);
		
		JLabel label_44 = new JLabel("Marca: ");
		label_44.setBounds(143, 84, 84, 14);
		eliminar_carro.add(label_44);
		
		JLabel label_45 = new JLabel("Modelo: ");
		label_45.setBounds(143, 109, 84, 14);
		eliminar_carro.add(label_45);
		
		JLabel label_46 = new JLabel("Cor: ");
		label_46.setBounds(143, 134, 84, 14);
		eliminar_carro.add(label_46);
		
		JLabel label_47 = new JLabel("Cilindrada: ");
		label_47.setBounds(143, 159, 84, 14);
		eliminar_carro.add(label_47);
		
		JLabel label_48 = new JLabel("Matricula: ");
		label_48.setBounds(143, 188, 84, 14);
		eliminar_carro.add(label_48);
		
		JLabel label_49 = new JLabel("Combust\u00EDvel: ");
		label_49.setBounds(143, 213, 98, 14);
		eliminar_carro.add(label_49);
		
		JLabel label_50 = new JLabel("Ano Aquisi\u00E7\u00E3o: ");
		label_50.setBounds(143, 238, 98, 14);
		eliminar_carro.add(label_50);
		
		JLabel label_51 = new JLabel("Pre\u00E7o Aquisi\u00E7\u00E3o: ");
		label_51.setBounds(143, 263, 119, 14);
		eliminar_carro.add(label_51);
		
		JLabel label_52 = new JLabel("Ano Venda: ");
		label_52.setBounds(143, 288, 98, 14);
		eliminar_carro.add(label_52);
		
		JLabel label_53 = new JLabel("Valor Venda: ");
		label_53.setBounds(143, 313, 98, 14);
		eliminar_carro.add(label_53);
		
		JLabel lblEliminarCarro = new JLabel("Eliminar Carro");
		lblEliminarCarro.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblEliminarCarro.setBounds(246, 11, 173, 36);
		eliminar_carro.add(lblEliminarCarro);
		
		JButton btn_sair_eliminar = new JButton("Sair");
		btn_sair_eliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				eliminar_carro.setVisible(false);
				menu.setVisible(true);
			}
		});
		btn_sair_eliminar.setBounds(514, 373, 89, 23);
		eliminar_carro.add(btn_sair_eliminar);
		
		JLabel label_55 = new JLabel("\u20AC");
		label_55.setBounds(414, 313, 46, 14);
		eliminar_carro.add(label_55);
		
		JLabel label_56 = new JLabel("\u20AC");
		label_56.setBounds(414, 263, 46, 14);
		eliminar_carro.add(label_56);
		
		

		JPanel menu_inserir = new JPanel();
		frmQwerty.getContentPane().add(menu_inserir, "name_19806605252847");
		menu_inserir.setLayout(null);
		
		JPanel menu_alterar = new JPanel();
		frmQwerty.getContentPane().add(menu_alterar, "name_9302888065562");
		menu_alterar.setLayout(null);
		
		JPanel alterar_carro = new JPanel();
		frmQwerty.getContentPane().add(alterar_carro, "name_9562476971791");
		alterar_carro.setLayout(null);
		
		JPanel alterar_inspecao = new JPanel();
		frmQwerty.getContentPane().add(alterar_inspecao, "name_9584274014217");
		alterar_inspecao.setLayout(null);
		
		JFormattedTextField txt_alt_data_insp = new JFormattedTextField();
		txt_alt_data_insp.setColumns(10);
		txt_alt_data_insp.setBounds(226, 138, 148, 20);
		alterar_inspecao.add(txt_alt_data_insp);
		adicionarMask("##/##/####", txt_alt_data_insp);
		
		JFormattedTextField txt_alt_valor_insp = new JFormattedTextField();
		txt_alt_valor_insp.setColumns(10);
		txt_alt_valor_insp.setBounds(225, 273, 148, 20);
		alterar_inspecao.add(txt_alt_valor_insp);
		
		JLabel label_14 = new JLabel("Matr\u00EDcula: ");
		label_14.setBounds(145, 110, 71, 14);
		alterar_inspecao.add(label_14);
		
		JLabel label_15 = new JLabel("Data de Inspe\u00E7\u00E3o: ");
		label_15.setBounds(107, 141, 109, 14);
		alterar_inspecao.add(label_15);
		
		JLabel label_16 = new JLabel("Observa\u00E7\u00F5es: ");
		label_16.setBounds(117, 174, 113, 14);
		alterar_inspecao.add(label_16);
		
		JLabel label_17 = new JLabel("Valor: ");
		label_17.setBounds(172, 276, 57, 14);
		alterar_inspecao.add(label_17);
		
		JLabel label_18 = new JLabel("Passou: ");
		label_18.setBounds(158, 312, 57, 14);
		alterar_inspecao.add(label_18);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(226, 169, 148, 93);
		alterar_inspecao.add(scrollPane_2);
		
		JTextArea txt_alt_obs_insp = new JTextArea();
		scrollPane_2.setViewportView(txt_alt_obs_insp);
		
		JFormattedTextField txt_alt_matr_insp = new JFormattedTextField();
		txt_alt_matr_insp.setColumns(10);
		txt_alt_matr_insp.setBounds(226, 107, 148, 20);
		alterar_inspecao.add(txt_alt_matr_insp);
		txt_alt_matr_insp.setEditable(false);
		adicionarMask("AA-AA-AA", txt_alt_matr_insp);
		
		JRadioButton rdb_alt_sim_insp = new JRadioButton("Sim");
		rdb_alt_sim_insp.setBounds(226, 308, 48, 23);
		alterar_inspecao.add(rdb_alt_sim_insp);
		
		
		JRadioButton rdb_alt_nao_insp = new JRadioButton("N\u00E3o");
		rdb_alt_nao_insp.setBounds(298, 308, 48, 23);
		alterar_inspecao.add(rdb_alt_nao_insp);
		
		bg_alt_insp.add(rdb_alt_sim_insp);
		bg_alt_insp.add(rdb_alt_nao_insp);
		
		JComboBox combo_id_insp = new JComboBox();
		
		JButton btn_alt_insp = new JButton("Guardar");
		btn_alt_insp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ResultSet rs = stmt.executeQuery("SELECT * FROM Inspecao");
					stmt = conn.createStatement();
					int tamanho_data = txt_alt_data_insp.getText().trim().length();
					if(rdb_alt_sim_insp.isSelected())
					{
						String Query="UPDATE Inspecao SET Matricula='"+txt_alt_matr_insp.getText()+"', DataInspec='"+txt_alt_data_insp.getText()+"', Obs='"+txt_alt_obs_insp.getText()+
							         "', Valor='"+txt_alt_valor_insp.getText()+"', Passou='Sim' WHERE IdInspecao LIKE '"+combo_id_insp.getSelectedItem()+"'";
						
						
						
						if(!txt_alt_valor_insp.getText().trim().isEmpty() && tamanho_data==10)
						{
							stmt.executeUpdate(Query);
							JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}
					}
					if(rdb_alt_nao_insp.isSelected())
					{
						String Query="UPDATE Inspecao SET Matricula='"+txt_alt_matr_insp.getText()+"', DataInspec='"+txt_alt_data_insp.getText()+"', Obs='"+txt_alt_obs_insp.getText()+
						         "', Valor='"+txt_alt_valor_insp.getText()+"', Passou='Nao' WHERE IdInspecao LIKE '"+combo_id_insp.getSelectedItem()+"'";
						
						if(!txt_alt_valor_insp.getText().trim().isEmpty() && tamanho_data==10)
								
						{
							System.out.println(Query);
							stmt.executeUpdate(Query);
							JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}
					}
					
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_alt_insp.setBounds(257, 362, 89, 23);
		alterar_inspecao.add(btn_alt_insp);
		
		JLabel lblAlterarInspeo = new JLabel("Alterar Inspe\u00E7\u00E3o");
		lblAlterarInspeo.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAlterarInspeo.setBounds(217, 11, 173, 36);
		alterar_inspecao.add(lblAlterarInspeo);
		
		JButton btn_alt_sair_insp = new JButton("Sair");
		btn_alt_sair_insp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				alterar_inspecao.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alt_sair_insp.setBounds(514, 373, 89, 23);
		alterar_inspecao.add(btn_alt_sair_insp);
		
		
		
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(168, 82, 48, 14);
		alterar_inspecao.add(lblId);
		
		
		combo_id_insp.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM Inspecao WHERE IdInspecao='"+combo_id_insp.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     while (rs.next())
				     {
				    	 txt_alt_matr_insp.setText(rs.getString("Matricula"));
				    	 txt_alt_data_insp.setText(rs.getString("DataInspec"));
				    	 txt_alt_obs_insp.setText(rs.getString("Obs"));
				    	 txt_alt_valor_insp.setText(rs.getString("Valor"));
				    	 String passou = rs.getString("Passou");
				    	 if(passou.equals("Sim"))
				    	 {
				    		 rdb_alt_sim_insp.setSelected(true);
				    		 rdb_alt_nao_insp.setSelected(false);
				    	 }
				    	 if(passou.equals("Nao"))
				    	 {
				    		 rdb_alt_sim_insp.setSelected(false);
				    		 rdb_alt_nao_insp.setSelected(true);
				    	 }
				     }
				     

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		combo_id_insp.setBounds(226, 79, 148, 20);
		alterar_inspecao.add(combo_id_insp);
		
		JLabel label_34 = new JLabel("\u20AC");
		label_34.setBounds(376, 276, 46, 14);
		alterar_inspecao.add(label_34);
		
		JPanel alterar_iuc = new JPanel();
		frmQwerty.getContentPane().add(alterar_iuc, "name_9588073048281");
		alterar_iuc.setLayout(null);
		
		JLabel lblAlterarIuc = new JLabel("Alterar IUC");
		lblAlterarIuc.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAlterarIuc.setBounds(248, 21, 173, 36);
		alterar_iuc.add(lblAlterarIuc);
		
		JLabel label_20 = new JLabel("Matr\u00EDcula: ");
		label_20.setBounds(156, 144, 71, 14);
		alterar_iuc.add(label_20);
		
		JLabel label_21 = new JLabel("Valor: ");
		label_21.setBounds(170, 184, 57, 14);
		alterar_iuc.add(label_21);
		
		JFormattedTextField txt_alt_valor_iuc = new JFormattedTextField();
		txt_alt_valor_iuc.setColumns(10);
		txt_alt_valor_iuc.setBounds(237, 181, 148, 17);
		alterar_iuc.add(txt_alt_valor_iuc);
		
		JFormattedTextField txt_alt_data_iuc = new JFormattedTextField();
		txt_alt_data_iuc.setColumns(10);
		txt_alt_data_iuc.setBounds(237, 218, 148, 17);
		alterar_iuc.add(txt_alt_data_iuc);
		adicionarMask("##/##/####", txt_alt_data_iuc);
		
		JFormattedTextField txt_alt_matr_iuc = new JFormattedTextField();
		txt_alt_matr_iuc.setColumns(10);
		txt_alt_matr_iuc.setBounds(237, 141, 148, 17);
		alterar_iuc.add(txt_alt_matr_iuc);
		adicionarMask("AA-AA-AA", txt_alt_matr_iuc);
		txt_alt_matr_iuc.setEditable(false);
		
		JLabel label_22 = new JLabel("Data de Validade: ");
		label_22.setBounds(121, 221, 106, 14);
		alterar_iuc.add(label_22);
		
		JComboBox combo_id_iuc = new JComboBox();
		
		JButton btn_alt_iuc = new JButton("Guardar");
		btn_alt_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ResultSet rs = stmt.executeQuery("SELECT * FROM IUC");
					stmt = conn.createStatement();
					String Query="UPDATE IUC SET Matricula='"+txt_alt_matr_iuc.getText()+
								 "', Valor='"+txt_alt_valor_iuc.getText()+
								 "', DataValidade='"+txt_alt_data_iuc.getText()+"' WHERE IdIuc LIKE '"+combo_id_iuc.getSelectedItem()+"'";
					
					int tamanho_data = txt_alt_data_iuc.getText().trim().length();
					if(!txt_alt_valor_iuc.getText().isEmpty() && tamanho_data==10 )
					{
						System.out.println(Query);
						stmt.executeUpdate(Query);
						JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				
			}
		});
		btn_alt_iuc.setBounds(265, 282, 89, 23);
		alterar_iuc.add(btn_alt_iuc);
		
		JButton btn_alt_sair_iuc = new JButton("Sair");
		btn_alt_sair_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				alterar_iuc.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alt_sair_iuc.setBounds(514, 373, 89, 23);
		alterar_iuc.add(btn_alt_sair_iuc);
		
		
		
		JLabel lblId_1 = new JLabel("ID:");
		lblId_1.setBounds(184, 112, 43, 14);
		alterar_iuc.add(lblId_1);
		
		
		combo_id_iuc.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM IUC WHERE IdIuc='"+combo_id_iuc.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     while (rs.next())
				     {
				    	 txt_alt_matr_iuc.setText(rs.getString("Matricula"));
				    	 txt_alt_valor_iuc.setText(rs.getString("Valor"));
				    	 txt_alt_data_iuc.setText(rs.getString("DataValidade"));
				     }
				     

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		combo_id_iuc.setBounds(237, 109, 148, 20);
		alterar_iuc.add(combo_id_iuc);
		
		JLabel label_35 = new JLabel("\u20AC");
		label_35.setBounds(387, 184, 46, 14);
		alterar_iuc.add(label_35);
		
		JPanel alterar_revisao = new JPanel();
		frmQwerty.getContentPane().add(alterar_revisao, "name_9594721514104");
		alterar_revisao.setLayout(null);
		
		JLabel lblAlterarReviso = new JLabel("Alterar Revis\u00E3o");
		lblAlterarReviso.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAlterarReviso.setBounds(233, 11, 173, 36);
		alterar_revisao.add(lblAlterarReviso);
		
		txt_alt_oficina_rev = new JTextField();
		txt_alt_oficina_rev.setColumns(10);
		txt_alt_oficina_rev.setBounds(232, 84, 148, 17);
		alterar_revisao.add(txt_alt_oficina_rev);
		
		JLabel label_23 = new JLabel("Oficina: ");
		label_23.setBounds(163, 87, 59, 14);
		alterar_revisao.add(label_23);
		
		JFormattedTextField txt_alt_kms_rev = new JFormattedTextField();
		txt_alt_kms_rev.setColumns(10);
		txt_alt_kms_rev.setBounds(232, 110, 148, 17);
		alterar_revisao.add(txt_alt_kms_rev);
		
		JLabel label_24 = new JLabel("KMS: ");
		label_24.setBounds(173, 112, 41, 14);
		alterar_revisao.add(label_24);
		
		JFormattedTextField txt_alt_data_rev = new JFormattedTextField();
		txt_alt_data_rev.setColumns(10);
		txt_alt_data_rev.setBounds(232, 139, 148, 17);
		alterar_revisao.add(txt_alt_data_rev);
		adicionarMask("##/##/####", txt_alt_data_rev);
		
		JLabel label_25 = new JLabel("Data de Revis\u00E3o: ");
		label_25.setBounds(116, 141, 100, 14);
		alterar_revisao.add(label_25);
		
		JLabel label_26 = new JLabel("Observa\u00E7\u00F5es: ");
		label_26.setBounds(136, 166, 86, 14);
		alterar_revisao.add(label_26);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(232, 167, 148, 126);
		alterar_revisao.add(scrollPane_3);
		
		JTextArea txt_alt_obs_rev = new JTextArea();
		scrollPane_3.setViewportView(txt_alt_obs_rev);
		
		JFormattedTextField txt_alt_matr_rev = new JFormattedTextField();
		txt_alt_matr_rev.setColumns(10);
		txt_alt_matr_rev.setBounds(232, 304, 148, 17);
		alterar_revisao.add(txt_alt_matr_rev);
		adicionarMask("AA-AA-AA", txt_alt_matr_rev);
		txt_alt_matr_rev.setEditable(false);
		
		JFormattedTextField txt_alt_valor_rev = new JFormattedTextField();
		txt_alt_valor_rev.setColumns(10);
		txt_alt_valor_rev.setBounds(232, 332, 148, 17);
		alterar_revisao.add(txt_alt_valor_rev);
		
		JComboBox combo_id_rev = new JComboBox();
		combo_id_rev.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM Revisao WHERE IdRevisao='"+combo_id_rev.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     while (rs.next())
				     {
				    	 txt_alt_oficina_rev.setText(rs.getString("Oficina"));
				    	 txt_alt_kms_rev.setText(rs.getString("Kms"));
				    	 txt_alt_data_rev.setText(rs.getString("DataRev"));
				    	 txt_alt_obs_rev.setText(rs.getString("Obs"));
				    	 txt_alt_matr_rev.setText(rs.getString("Matricula"));
				    	 txt_alt_valor_rev.setText(rs.getString("Valor"));
				     }
				     

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		combo_id_rev.setBounds(233, 53, 148, 20);
		alterar_revisao.add(combo_id_rev);
		
		JLabel label_27 = new JLabel("Matricula: ");
		label_27.setBounds(155, 306, 67, 14);
		alterar_revisao.add(label_27);
		
		JLabel label_28 = new JLabel("Valor: ");
		label_28.setBounds(173, 334, 41, 14);
		alterar_revisao.add(label_28);
		
		JButton btn_alt_rev = new JButton("Guardar");
		btn_alt_rev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ResultSet rs = stmt.executeQuery("SELECT * FROM Revisao");
					stmt = conn.createStatement();
					String Query="UPDATE Revisao SET Oficina='"+txt_alt_oficina_rev.getText()+
								 "', Kms='"+txt_alt_kms_rev.getText()+
								 "', DataRev='"+txt_alt_data_rev.getText()+
								 "', Obs='"+txt_alt_obs_rev.getText()+
								 "', Matricula='"+txt_alt_matr_rev.getText()+
								 "', Valor='"+txt_alt_valor_rev.getText()+"' WHERE IdRevisao LIKE '"+combo_id_rev.getSelectedItem()+"'";
					
					int tamanho_data = txt_alt_data_rev.getText().trim().length();
					if(!txt_alt_oficina_rev.getText().isEmpty() && 
							!txt_alt_kms_rev.getText().trim().isEmpty() &&
							!txt_alt_valor_rev.getText().trim().isEmpty() && tamanho_data==10)
					{
						System.out.println(Query);
						stmt.executeUpdate(Query);
						JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_alt_rev.setBounds(263, 360, 89, 23);
		alterar_revisao.add(btn_alt_rev);
		
		JButton btn_alt_sair_rev = new JButton("Sair");
		btn_alt_sair_rev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				alterar_revisao.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alt_sair_rev.setBounds(514, 373, 89, 23);
		alterar_revisao.add(btn_alt_sair_rev);
		
		
		
		JLabel lblId_2 = new JLabel("ID:");
		lblId_2.setBounds(181, 56, 41, 14);
		alterar_revisao.add(lblId_2);
		
		JLabel label_36 = new JLabel("\u20AC");
		label_36.setBounds(383, 334, 46, 14);
		alterar_revisao.add(label_36);
		
		JPanel alterar_seguro = new JPanel();
		frmQwerty.getContentPane().add(alterar_seguro, "name_9604339043421");
		alterar_seguro.setLayout(null);
		
		JLabel lblAlterarSeguro = new JLabel("Alterar Seguro");
		lblAlterarSeguro.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAlterarSeguro.setBounds(233, 11, 173, 36);
		alterar_seguro.add(lblAlterarSeguro);
		
		txt_alt_nome_seg = new JTextField();
		txt_alt_nome_seg.setColumns(10);
		txt_alt_nome_seg.setBounds(243, 93, 148, 17);
		alterar_seguro.add(txt_alt_nome_seg);
		
		JLabel label_29 = new JLabel("Nome: ");
		label_29.setBounds(176, 96, 57, 14);
		alterar_seguro.add(label_29);
		
		JLabel label_30 = new JLabel("Data de Validade: ");
		label_30.setBounds(127, 122, 106, 14);
		alterar_seguro.add(label_30);
		
		JFormattedTextField txt_alt_data_seg = new JFormattedTextField();
		txt_alt_data_seg.setColumns(10);
		txt_alt_data_seg.setBounds(243, 119, 148, 17);
		alterar_seguro.add(txt_alt_data_seg);
		adicionarMask("##/##/####", txt_alt_data_seg);
		
		JFormattedTextField txt_alt_valor_seg = new JFormattedTextField();
		txt_alt_valor_seg.setColumns(10);
		txt_alt_valor_seg.setBounds(242, 147, 148, 17);
		alterar_seguro.add(txt_alt_valor_seg);
		
		JLabel label_31 = new JLabel("Valor: ");
		label_31.setBounds(175, 150, 57, 14);
		alterar_seguro.add(label_31);
		
		JLabel label_32 = new JLabel("Matr\u00EDcula: ");
		label_32.setBounds(162, 178, 71, 14);
		alterar_seguro.add(label_32);
		
		JFormattedTextField txt_alt_matr_seg = new JFormattedTextField();
		txt_alt_matr_seg.setColumns(10);
		txt_alt_matr_seg.setBounds(243, 175, 148, 17);
		alterar_seguro.add(txt_alt_matr_seg);
		txt_alt_matr_seg.setEditable(false);
		
		
		JComboBox combo_id_seg = new JComboBox();
		combo_id_seg.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM Seguro WHERE IdSeguro='"+combo_id_seg.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     while (rs.next())
				     {
				    	 txt_alt_nome_seg.setText(rs.getString("Nome"));
				    	 txt_alt_data_seg.setText(rs.getString("DataValidade"));
				    	 txt_alt_valor_seg.setText(rs.getString("Valor"));
				    	 txt_alt_matr_seg.setText(rs.getString("Matricula"));
				     }
				     

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				
			}
		});
		combo_id_seg.setBounds(243, 62, 148, 20);
		alterar_seguro.add(combo_id_seg);
		
		JButton btn_alt_seg = new JButton("Guardar");
		btn_alt_seg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					stmt = conn.createStatement();
					String Query="UPDATE Seguro SET Nome='"+txt_alt_nome_seg.getText()+
								 "', DataValidade='"+txt_alt_data_seg.getText()+
								 "', Valor='"+txt_alt_valor_seg.getText()+
								 "', Matricula='"+txt_alt_matr_seg.getText()+"' WHERE IdSeguro LIKE '"+combo_id_seg.getSelectedItem()+"'";
					
					int tamanho_data = txt_alt_data_seg.getText().trim().length();
					if(!txt_alt_nome_seg.getText().isEmpty() && 
							!txt_alt_valor_seg.getText().trim().isEmpty() &&  tamanho_data==10)
					{
						System.out.println(Query);
						stmt.executeUpdate(Query);
						JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_alt_seg.setBounds(271, 206, 89, 23);
		alterar_seguro.add(btn_alt_seg);
		
		JButton btn_alt_sair_seg = new JButton("Sair");
		btn_alt_sair_seg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				alterar_seguro.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alt_sair_seg.setBounds(514, 373, 89, 23);
		alterar_seguro.add(btn_alt_sair_seg);
		
		JLabel lblId_3 = new JLabel("ID: ");
		lblId_3.setBounds(187, 65, 46, 14);
		alterar_seguro.add(lblId_3);
		
		JLabel label_37 = new JLabel("\u20AC");
		label_37.setBounds(392, 149, 46, 14);
		alterar_seguro.add(label_37);
		
		
		
		JComboBox combo_alt_combustivel = new JComboBox();
		combo_alt_combustivel.setBounds(260, 210, 149, 20);
		alterar_carro.add(combo_alt_combustivel);
		
		txt_alt_marca = new JTextField();
		txt_alt_marca.setColumns(10);
		txt_alt_marca.setBounds(260, 81, 149, 20);
		alterar_carro.add(txt_alt_marca);
		
		txt_alt_modelo = new JTextField();
		txt_alt_modelo.setColumns(10);
		txt_alt_modelo.setBounds(260, 106, 149, 20);
		alterar_carro.add(txt_alt_modelo);
		
		txt_alt_cor = new JTextField();
		txt_alt_cor.setColumns(10);
		txt_alt_cor.setBounds(260, 131, 149, 20);
		alterar_carro.add(txt_alt_cor);
		
		JFormattedTextField txt_alt_cilindrada = new JFormattedTextField();
		txt_alt_cilindrada.setColumns(10);
		txt_alt_cilindrada.setBounds(260, 156, 149, 20);
		alterar_carro.add(txt_alt_cilindrada);
		adicionarMask("####", txt_alt_cilindrada);
		
		JFormattedTextField txt_alt_ano_aqui = new JFormattedTextField();
		txt_alt_ano_aqui.setColumns(10);
		txt_alt_ano_aqui.setBounds(260, 235, 149, 20);
		alterar_carro.add(txt_alt_ano_aqui);
		adicionarMask("####", txt_alt_ano_aqui);
		
		JFormattedTextField txt_alt_preco_aqui = new JFormattedTextField();
		txt_alt_preco_aqui.setColumns(10);
		txt_alt_preco_aqui.setBounds(260, 260, 149, 20);
		alterar_carro.add(txt_alt_preco_aqui);
		
		
		JFormattedTextField txt_alt_ano_venda = new JFormattedTextField();
		txt_alt_ano_venda.setColumns(10);
		txt_alt_ano_venda.setBounds(260, 285, 149, 20);
		alterar_carro.add(txt_alt_ano_venda);
		adicionarMask("####", txt_alt_ano_venda);
		
		JFormattedTextField txt_alt_valor_venda = new JFormattedTextField();
		txt_alt_valor_venda.setColumns(10);
		txt_alt_valor_venda.setBounds(260, 310, 149, 20);
		
		
		alterar_carro.add(txt_alt_valor_venda);
		JComboBox combo_alt_matricula = new JComboBox();
		combo_alt_matricula.addItemListener(new ItemListener() {
			
			public void itemStateChanged(ItemEvent arg0) {
				try {
					stmt = conn.createStatement();
				     String query = "SELECT * FROM Carro WHERE Matricula='"+combo_alt_matricula.getSelectedItem()+"'";
				     ResultSet rs = stmt.executeQuery(query);
				     String combustivel ="";
				     while (rs.next())
				     {
				    	 txt_alt_marca.setText(rs.getString("Marca"));
				    	 txt_alt_modelo.setText(rs.getString("Modelo"));
				    	 txt_alt_cor.setText(rs.getString("Cor"));
				    	 txt_alt_cilindrada.setText(rs.getString("Cilindrada"));
				    	 txt_alt_ano_aqui.setText(rs.getString("AnoAquisicao"));
				    	 txt_alt_preco_aqui.setText(rs.getString("PrecoAquisicao"));
				    	 txt_alt_ano_venda.setText(rs.getString("AnoVenda"));
				    	 txt_alt_valor_venda.setText(rs.getString("ValorVenda"));
				    	 combustivel=rs.getString("Combustivel");
			    	 
				     }
				     rs = stmt.executeQuery("SELECT * FROM Combustiveis");
				     combo_alt_combustivel.removeAllItems();
				     while (rs.next())
				      combo_alt_combustivel.addItem(rs.getString(1));
				     combo_alt_combustivel.setSelectedItem(combustivel);
				     
						

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}

			}
		});
		
		
		
		JButton btn_alt_carro = new JButton("Guardar");
		btn_alt_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
					stmt = conn.createStatement();
					String Query="UPDATE Carro SET Marca='"+txt_alt_marca.getText()+"', Modelo='"+txt_alt_modelo.getText()+"', Cor='"+txt_alt_cor.getText()+
							       "', Cilindrada='"+txt_alt_cilindrada.getText()+"', Combustivel='"+combo_alt_combustivel.getSelectedItem()+
							       "', AnoAquisicao='"+txt_alt_ano_aqui.getText()+"', PrecoAquisicao='"+txt_alt_preco_aqui.getText()+"', AnoVenda='"+txt_alt_ano_venda.getText()+
							       "', ValorVenda='"+txt_alt_valor_venda.getText()+"' WHERE Matricula LIKE '"+combo_alt_matricula.getSelectedItem()+"'";
					
					if(!txt_alt_marca.getText().isEmpty() && 
							!txt_alt_modelo.getText().isEmpty() &&
							!txt_alt_cor.getText().isEmpty() && 
							!txt_alt_cilindrada.getText().trim().isEmpty() &&  
							!txt_alt_ano_aqui.getText().trim().isEmpty() && 
							!txt_alt_preco_aqui.getText().trim().isEmpty())
					{
						stmt.executeUpdate(Query);
						JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}

			}
		});
		btn_alt_carro.setBounds(292, 353, 89, 23);
		alterar_carro.add(btn_alt_carro);
		
		combo_alt_matricula.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
			}
		});
		combo_alt_matricula.setBounds(260, 185, 149, 20);
		alterar_carro.add(combo_alt_matricula);
		
		JButton btn_alterar_carro = new JButton("Alterar Carro");
		btn_alterar_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					if(combo_alt_matricula.getItemCount()==0 && combo_alt_combustivel.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");

						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_alt_matricula.addItem(matr);
							
						}
						ResultSet rc = stmt.executeQuery("SELECT * FROM Combustiveis");
						while(rc.next())
						{
							Combustiveis comb = new Combustiveis(rc.getString("NomeCombustivel"));
							combo_alt_combustivel.addItem(comb);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_alterar.setVisible(false);
				alterar_carro.setVisible(true);	
			}
		});
		btn_alterar_carro.setBounds(113, 161, 131, 23);
		menu_alterar.add(btn_alterar_carro);
		
		JButton btn_alterar_insp = new JButton("Alterar Inspe\u00E7\u00E3o");
		btn_alterar_insp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_id_insp.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Inspecao");

						while(rs.next())
						{
							Id id = new Id(rs.getString("IdInspecao"));
							combo_id_insp.addItem(id);
						}
						
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_alterar.setVisible(false);
				alterar_inspecao.setVisible(true);
			}
		});
		btn_alterar_insp.setBounds(247, 161, 131, 23);
		menu_alterar.add(btn_alterar_insp);
		
		JButton btn_alterar_iuc = new JButton("Alterar IUC");
		btn_alterar_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_id_iuc.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM IUC");

						while(rs.next())
						{
							Id id = new Id(rs.getString("IdIuc"));
							combo_id_iuc.addItem(id);
						}
						
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_alterar.setVisible(false);
				alterar_iuc.setVisible(true);
			}
		});
		btn_alterar_iuc.setBounds(382, 161, 102, 23);
		menu_alterar.add(btn_alterar_iuc);
		
		JButton btn_alterar_seguro = new JButton("Alterar Seguro");
		btn_alterar_seguro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_id_seg.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Seguro");

						while(rs.next())
						{
							Id id = new Id(rs.getString("IdSeguro"));
							combo_id_seg.addItem(id);
						}
						
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_alterar.setVisible(false);
				alterar_seguro.setVisible(true);
			}
		});
		btn_alterar_seguro.setBounds(330, 212, 119, 23);
		menu_alterar.add(btn_alterar_seguro);
		
		JButton btn_alterar_revisao = new JButton("Alterar Revis\u00E3o");
		btn_alterar_revisao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_id_rev.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Revisao");

						while(rs.next())
						{
							Id id = new Id(rs.getString("IdRevisao"));
							combo_id_rev.addItem(id);
						}
						
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_alterar.setVisible(false);
				alterar_revisao.setVisible(true);
			}
		});
		btn_alterar_revisao.setBounds(177, 212, 131, 23);
		menu_alterar.add(btn_alterar_revisao);
		
		JButton btn_sair_menu_alt = new JButton("Sair");
		btn_sair_menu_alt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				menu_alterar.setVisible(false);
				menu.setVisible(true);
			}
		});
		btn_sair_menu_alt.setBounds(514, 373, 89, 23);
		menu_alterar.add(btn_sair_menu_alt);

		JPanel inserir_carro = new JPanel();
		frmQwerty.getContentPane().add(inserir_carro, "name_20630381807918");
		inserir_carro.setLayout(null);

		JPanel inserir_inspecao = new JPanel();
		frmQwerty.getContentPane().add(inserir_inspecao, "name_2306569758309");
		inserir_inspecao.setLayout(null);

		JPanel inserir_iuc = new JPanel();
		frmQwerty.getContentPane().add(inserir_iuc, "name_11170099444361");
		inserir_iuc.setLayout(null);

		JPanel inserir_revisao = new JPanel();
		frmQwerty.getContentPane().add(inserir_revisao, "name_14705241122408");
		inserir_revisao.setLayout(null);

		JPanel inserir_seguro = new JPanel();
		frmQwerty.getContentPane().add(inserir_seguro, "name_17916501466971");
		inserir_seguro.setLayout(null);
		
		

		txt_data_insp = new JFormattedTextField();
		adicionarMask("##/##/####", txt_data_insp);
		txt_data_insp.setColumns(10);
		txt_data_insp.setBounds(225, 105, 148, 20);
		inserir_inspecao.add(txt_data_insp);

		txt_valor_insp = new JFormattedTextField();
		adicionarMask("###", txt_valor_insp);
		txt_valor_insp.setColumns(10);
		txt_valor_insp.setBounds(225, 273, 148, 20);
		inserir_inspecao.add(txt_valor_insp);

		JLabel lblNewLabel_1 = new JLabel("Matr\u00EDcula: ");
		lblNewLabel_1.setBounds(144, 77, 71, 14);
		inserir_inspecao.add(lblNewLabel_1);

		JLabel lblDataDeInsoeo = new JLabel("Data de Inspe\u00E7\u00E3o: ");
		lblDataDeInsoeo.setBounds(106, 108, 109, 14);
		inserir_inspecao.add(lblDataDeInsoeo);

		JLabel lblObservaes = new JLabel("Observa\u00E7\u00F5es: ");
		lblObservaes.setBounds(116, 141, 113, 14);
		inserir_inspecao.add(lblObservaes);

		JLabel lblValor = new JLabel("Valor: ");
		lblValor.setBounds(172, 276, 57, 14);
		inserir_inspecao.add(lblValor);

		JLabel lblPassou = new JLabel("Passou: ");
		lblPassou.setBounds(158, 312, 57, 14);
		inserir_inspecao.add(lblPassou);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(225, 136, 148, 126);
		inserir_inspecao.add(scrollPane_4);

		JTextArea textArea_obs = new JTextArea();
		scrollPane_4.setViewportView(textArea_obs);

		JComboBox<Matricula> combo_matricula_insp = new JComboBox();
		combo_matricula_insp.setBounds(225, 74, 148, 20);
		inserir_inspecao.add(combo_matricula_insp);

		JComboBox<Matricula> combo_matricula_iuc = new JComboBox();
		combo_matricula_iuc.setBounds(237, 101, 148, 20);
		inserir_iuc.add(combo_matricula_iuc);

		JComboBox<Matricula> combo_matricula_rev = new JComboBox();
		combo_matricula_rev.setBounds(233, 281, 148, 20);
		inserir_revisao.add(combo_matricula_rev);

		JComboBox<Matricula> combo_matricula_seg = new JComboBox();
		combo_matricula_seg.setBounds(243, 175, 148, 20);
		inserir_seguro.add(combo_matricula_seg);
		

		JRadioButton rdbtn_sim = new JRadioButton("Sim");
		rdbtn_sim.setBounds(226, 308, 48, 23);
		inserir_inspecao.add(rdbtn_sim);

		JRadioButton rdbtn_nao = new JRadioButton("N\u00E3o");
		rdbtn_nao.setBounds(298, 308, 48, 23);
		inserir_inspecao.add(rdbtn_nao);

		bg.add(rdbtn_sim);
		bg.add(rdbtn_nao);

		JButton btn_guardar_inspecao = new JButton("Guardar");
		btn_guardar_inspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {

					stmt = conn.createStatement();
					int tamanho = txt_data_insp.getText().trim().length();
					System.out.println(tamanho);
					if(rdbtn_sim.isSelected() && !txt_data_insp.getText().isEmpty() && !textArea_obs.getText().isEmpty() && !txt_valor_insp.getText().isEmpty() && tamanho == 10)
					{
						String query = "INSERT INTO Inspecao (Matricula, DataInspec, Obs, Valor, Passou) VALUES ('"+ combo_matricula_insp.getSelectedItem() + 
										"' , '" + txt_data_insp.getText() +
										"' , '" + textArea_obs.getText() + 
										"' , '" + txt_valor_insp.getText() + 
										"' , 'Sim')";					

						stmt.executeUpdate(query);
						JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						if(rdbtn_nao.isSelected() && !txt_data_insp.getText().isEmpty() && !textArea_obs.getText().isEmpty() && !txt_valor_insp.getText().isEmpty() && tamanho == 10)
						{
							String query = "INSERT INTO Inspecao VALUES ('"+ combo_matricula_insp.getSelectedItem() + 
											"' , '" + txt_data_insp.getText() +
											"' , '" + textArea_obs.getText() + 
											"' , '" + txt_valor_insp.getText() + 
											"' , 'Nao')";
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}
					}


				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				txt_marca.setText("");
				txt_modelo.setText("");
				txt_cor.setText("");
				txt_cilindrada.setText("");
				txt_matricula.setText("");


			}
		});
		btn_guardar_inspecao.setBounds(257, 362, 89, 23);
		inserir_inspecao.add(btn_guardar_inspecao);

		JButton btn_sair_inspecao = new JButton("Sair");
		btn_sair_inspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txt_marca.setText("");
				txt_modelo.setText("");
				txt_cor.setText("");
				txt_cilindrada.setText("");
				txt_matricula.setText("");
				inserir_inspecao.setVisible(false);
				menu_inserir.setVisible(true);


			}
		});
		btn_sair_inspecao.setBounds(514, 373, 89, 23);
		inserir_inspecao.add(btn_sair_inspecao);

		JLabel lblInserirInspeo = new JLabel("Inserir Inspe\u00E7\u00E3o");
		lblInserirInspeo.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblInserirInspeo.setBounds(217, 11, 173, 36);
		inserir_inspecao.add(lblInserirInspeo);
		
		JLabel label_40 = new JLabel("\u20AC");
		label_40.setBounds(376, 276, 46, 14);
		inserir_inspecao.add(label_40);











		JComboBox<Combustiveis> combo_combustivel = new JComboBox();
		combo_combustivel.setBounds(260, 210, 149, 20);
		inserir_carro.add(combo_combustivel);

		JButton btn_sair_menu_inserir = new JButton("Sair");
		btn_sair_menu_inserir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				menu_inserir.setVisible(false);
				menu.setVisible(true);
			}
		});
		btn_sair_menu_inserir.setBounds(511, 371, 89, 23);
		menu_inserir.add(btn_sair_menu_inserir);

		JButton btn_inserir_carro = new JButton("Inserir Carro");
		btn_inserir_carro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_inserir_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_combustivel.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Combustiveis");

						while(rs.next())
						{
							Combustiveis comb = new Combustiveis(rs.getString("NomeCombustivel"));
							combo_combustivel.addItem(comb);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};

				menu_inserir.setVisible(false);
				inserir_carro.setVisible(true);

			}
		});
		btn_inserir_carro.setBounds(110, 159, 131, 23);
		menu_inserir.add(btn_inserir_carro);

		JButton btn_inserir_inspecao = new JButton("Inserir Inspe\u00E7\u00E3o");
		btn_inserir_inspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					if(combo_matricula_insp.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT Matricula FROM Carro");

						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_matricula_insp.addItem(matr);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_inserir.setVisible(false);
				inserir_inspecao.setVisible(true);	
			}
		});
		btn_inserir_inspecao.setBounds(244, 159, 131, 23);
		menu_inserir.add(btn_inserir_inspecao);

		JButton btn_inserir_iuc = new JButton("Inserir IUC");
		btn_inserir_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					if(combo_matricula_iuc.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT Matricula FROM Carro");

						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_matricula_iuc.addItem(matr);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_inserir.setVisible(false);
				inserir_iuc.setVisible(true);

			}
		});
		btn_inserir_iuc.setBounds(379, 159, 102, 23);
		menu_inserir.add(btn_inserir_iuc);

		JButton btnInserirReviso = new JButton("Inserir Revis\u00E3o");
		btnInserirReviso.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_matricula_rev.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT Matricula FROM Carro");

						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_matricula_rev.addItem(matr);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_inserir.setVisible(false);
				inserir_revisao.setVisible(true);
			}
		});
		btnInserirReviso.setBounds(186, 210, 119, 23);
		menu_inserir.add(btnInserirReviso);

		JButton btn_inserir_seg = new JButton("Inserir Seguro");
		btn_inserir_seg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if(combo_matricula_seg.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT Matricula FROM Carro");

						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_matricula_seg.addItem(matr);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu_inserir.setVisible(false);
				inserir_seguro.setVisible(true);


			}
		});
		btn_inserir_seg.setBounds(327, 210, 119, 23);
		menu_inserir.add(btn_inserir_seg);

		JLabel lblAsdada = new JLabel("");
		lblAsdada.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAsdada.setBounds(363, 218, 171, 43);
		login.add(lblAsdada);

		btn_login = new JButton("Login");
		btn_login.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {


				try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Users");

					while (rs.next())
					{
						String user = rs.getString(2);
						String pass = rs.getString(3);
						System.out.println(user);
						System.out.println(pass);
						if(txt_user.getText().equals(user) && (pass.equals(new String((passwordField.getPassword())))))
						{
							lblAsdada.setText("");
							login.setVisible(false);
							menu.setVisible(true);
						}
						else
						{
							lblAsdada.setForeground(Color.RED);
							lblAsdada.setText("Dados de Login errados!");
							//txt_user.setText("");
							//passwordField.setText("");
						}
					}
				} catch (SQLException se) {
					// Handle errors for JDBC
					se.printStackTrace();
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}			
		});
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_login.setBounds(397, 195, 96, 23);
		login.add(btn_login);

		JLabel label_image = new JLabel("");
		java.awt.Image img = new ImageIcon(this.getClass().getResource("/form-login.png")).getImage();
		label_image.setIcon(new ImageIcon(img));

		label_image.setBounds(10, 72, 282, 234);
		login.add(label_image);

		JButton btn_anonimo = new JButton("An\u00F3nimo");
		btn_anonimo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				anonimo.setVisible(true);
				login.setVisible(false);
				try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
					table_carros_anonimo.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_anonimo.setBounds(397, 323, 96, 23);
		login.add(btn_anonimo);

		JButton btn_registar_user = new JButton("Registar");
		btn_registar_user.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registar.setVisible(true);
				login.setVisible(false);
			}
		});
		btn_registar_user.setBounds(397, 300, 96, 22);
		login.add(btn_registar_user);



		btn_sair = new JButton("Sair");
		btn_sair.setBounds(540, 373, 63, 23);
		btn_sair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				menu.setVisible(false);
				login.setVisible(true);
				txt_user.setText("");
				passwordField.setText("");


			}
		});
		menu.setLayout(null);
		menu.add(btn_sair);


		//Botao Consultar

		JButton btn_consultar = new JButton("Consultar todos os Dados");
		btn_consultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				menu.setVisible(false);
				consultar.setVisible(true);





			}
		});
		btn_consultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_consultar.setBounds(42, 190, 184, 23);
		menu.add(btn_consultar);

		JButton btn_inserir = new JButton("Inserir Dados");
		btn_inserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_inserir.addMouseListener(new MouseAdapter() {
			@Override
			//Botao Inserir
			public void mouseClicked(MouseEvent e) {
				menu.setVisible(false);
				menu_inserir.setVisible(true);




			}
		});
		btn_inserir.setBounds(246, 190, 124, 23);
		menu.add(btn_inserir);

		JLabel label_img_menu = new JLabel("");
		label_img_menu.setHorizontalAlignment(SwingConstants.CENTER);
		label_img_menu.setBounds(0, 0, 610, 145);
		menu.add(label_img_menu);

		java.awt.Image img_menu = new ImageIcon(this.getClass().getResource("/menu.png")).getImage();
		label_img_menu.setIcon(new ImageIcon(img_menu));
		
		JButton btn_alterar = new JButton("Alterar Dados");
		btn_alterar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				menu.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alterar.setBounds(380, 190, 124, 23);
		menu.add(btn_alterar);
		
		JButton btn_eliminar_carro = new JButton("Eliminar Carro");
		btn_eliminar_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					combo_eliminar_matr.removeAllItems();
					if(combo_eliminar_matr.getItemCount()==0)
					{
						stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
						while(rs.next())
						{
							Matricula matr = new Matricula(rs.getString("Matricula"));
							combo_eliminar_matr.addItem(matr);
						}
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				};
				menu.setVisible(false);
				eliminar_carro.setVisible(true);
			}
		});
		btn_eliminar_carro.setBounds(246, 243, 124, 23);
		menu.add(btn_eliminar_carro);



		JButton btn_sair_consulta = new JButton("Sair");
		btn_sair_consulta.setBounds(536, 371, 64, 23);
		btn_sair_consulta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				consultar.setVisible(false);
				menu.setVisible(true);


			}
		});
		consultar.setLayout(null);
		consultar.add(btn_sair_consulta);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 62, 590, 248);
		consultar.add(scrollPane);

		table_dados = new JTable(){
			public boolean isCellEditable(int rowIndex, int mColIndex)
			{
				if(mColIndex == 4)
					return false;
				return true;
			}
		};
		scrollPane.setViewportView(table_dados);
		

		JButton btn_carros = new JButton("Carros");
		btn_carros.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));
					System.out.println("");
					table_dados.getModel();

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_carros.setBounds(10, 11, 89, 23);
		consultar.add(btn_carros);

		JButton btn_combustveis = new JButton("Combust\u00EDveis");
		btn_combustveis.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {


				try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Combustiveis");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_combustveis.setBounds(100, 11, 117, 23);
		consultar.add(btn_combustveis);

		JButton btn_inspecao = new JButton("Inspe\u00E7\u00E3o");
		btn_inspecao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_inspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {


				try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Inspecao");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_inspecao.setBounds(218, 11, 89, 23);
		consultar.add(btn_inspecao);

		JButton btn_iuc = new JButton("IUC");
		btn_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {


				try {

					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM IUC");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_iuc.setBounds(307, 11, 89, 23);
		consultar.add(btn_iuc);

		JButton btn_revisao = new JButton("Revis\u00E3o");
		btn_revisao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_revisao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {


				try {

					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Revisao");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_revisao.setBounds(396, 11, 89, 23);
		consultar.add(btn_revisao);

		JButton btn_seguro = new JButton("Seguros");
		btn_seguro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {


				try {

					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Seguro");
					table_dados.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		btn_seguro.setBounds(487, 11, 89, 23);
		consultar.add(btn_seguro);



		JLabel lblNewLabel = new JLabel("Marca: ");
		lblNewLabel.setBounds(143, 84, 84, 14);
		inserir_carro.add(lblNewLabel);

		JLabel lblModelo = new JLabel("Modelo: ");
		lblModelo.setBounds(143, 109, 84, 14);
		inserir_carro.add(lblModelo);

		JLabel lblCor = new JLabel("Cor: ");
		lblCor.setBounds(143, 134, 84, 14);
		inserir_carro.add(lblCor);

		JLabel lblCilindrada = new JLabel("Cilindrada: ");
		lblCilindrada.setBounds(143, 159, 84, 14);
		inserir_carro.add(lblCilindrada);

		JLabel lblMatricula = new JLabel("Matricula: ");
		lblMatricula.setBounds(143, 188, 84, 14);
		inserir_carro.add(lblMatricula);

		JLabel lblCombustvel = new JLabel("Combust\u00EDvel: ");
		lblCombustvel.setBounds(143, 213, 98, 14);
		inserir_carro.add(lblCombustvel);

		JLabel lblAnoAquisio = new JLabel("Ano Aquisi\u00E7\u00E3o: ");
		lblAnoAquisio.setBounds(143, 238, 98, 14);
		inserir_carro.add(lblAnoAquisio);

		JLabel lblPreoAquisio = new JLabel("Pre\u00E7o Aquisi\u00E7\u00E3o: ");
		lblPreoAquisio.setBounds(143, 263, 119, 14);
		inserir_carro.add(lblPreoAquisio);

		JLabel lblAnoVenda = new JLabel("Ano Venda: ");
		lblAnoVenda.setBounds(143, 288, 98, 14);
		inserir_carro.add(lblAnoVenda);

		JLabel lblValorVenda = new JLabel("Valor Venda: ");
		lblValorVenda.setBounds(143, 313, 98, 14);
		inserir_carro.add(lblValorVenda);

		txt_marca = new JTextField();
		txt_marca.setBounds(260, 81, 149, 20);
		inserir_carro.add(txt_marca);
		txt_marca.setColumns(10);

		txt_modelo = new JTextField();
		txt_modelo.setColumns(10);
		txt_modelo.setBounds(260, 106, 149, 20);
		inserir_carro.add(txt_modelo);

		txt_cor = new JTextField();
		txt_cor.setColumns(10);
		txt_cor.setBounds(260, 131, 149, 20);
		inserir_carro.add(txt_cor);

		txt_cilindrada = new JFormattedTextField();
		adicionarMask("####", txt_cilindrada);
		txt_cilindrada.setColumns(10);
		txt_cilindrada.setBounds(260, 156, 149, 20);
		inserir_carro.add(txt_cilindrada);

		txt_matricula = new JFormattedTextField();
		txt_matricula.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if(Character.isLowerCase(arg0.getKeyChar()))
				     arg0.setKeyChar(Character.toUpperCase(arg0.getKeyChar()));
			}
		});
		adicionarMask("AA-AA-AA", txt_matricula);
		txt_matricula.setColumns(10);
		txt_matricula.setBounds(260, 185, 149, 20);
		inserir_carro.add(txt_matricula);

		txt_ano_aqui = new JFormattedTextField();
		adicionarMask("####", txt_ano_aqui);
		txt_ano_aqui.setColumns(10);
		txt_ano_aqui.setBounds(260, 235, 149, 20);
		inserir_carro.add(txt_ano_aqui);

		txt_preco_aqui = new JFormattedTextField();
		adicionarMask("##################", txt_preco_aqui);
		txt_preco_aqui.setColumns(10);
		txt_preco_aqui.setBounds(260, 260, 149, 20);
		inserir_carro.add(txt_preco_aqui);

		txt_ano_venda = new JFormattedTextField();
		adicionarMask("####", txt_ano_venda);
		txt_ano_venda.setColumns(10);
		txt_ano_venda.setBounds(260, 285, 149, 20);
		inserir_carro.add(txt_ano_venda);

		txt_valor_venda = new JFormattedTextField();
		adicionarMask("##################", txt_valor_venda);
		txt_valor_venda.setColumns(10);
		txt_valor_venda.setBounds(260, 310, 149, 20);
		inserir_carro.add(txt_valor_venda);

		JButton btn_guardar_carro = new JButton("Guardar");
		btn_guardar_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					ResultSet rs = stmt.executeQuery("SELECT * FROM Carro");
					stmt = conn.createStatement();
					int i = 0;
					while(rs.next())
					{
						String novoCarro = rs.getString(5);
						if(novoCarro.equals(txt_matricula.getText()))
						{
							i++;	
						}
					}
					if(i==1)
					{
						JOptionPane.showMessageDialog(null, "Essa matricula ja existe", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						txt_matricula.setText("");
					}
					else
					{

						String query = "INSERT INTO Carro VALUES ('"+ txt_marca.getText() + 
								"' , '" + txt_modelo.getText() + 
								"' , '" + txt_cor.getText() + 
								"' , '" + txt_cilindrada.getText() + 
								"' , '" + txt_matricula.getText() + 
								"' , '" + combo_combustivel.getSelectedItem() + 
								"' , '" + txt_ano_aqui.getText() + 
								"' , '" + txt_preco_aqui.getText() + 
								"' , '" + txt_ano_venda.getText() + 
								"' , '" + txt_valor_venda.getText() +"')";


						int tamanho = txt_matricula.getText().trim().length();
						System.out.println(tamanho);
						if(!txt_modelo.getText().isEmpty() && 
								!txt_cor.getText().isEmpty() && 
								!txt_cilindrada.getText().trim().isEmpty() && 
								!txt_matricula.getText().trim().isEmpty() && 
								!txt_ano_aqui.getText().trim().isEmpty() &&
								!txt_preco_aqui.getText().trim().isEmpty() && tamanho == 8)
						{
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
							txt_marca.setText("");
							txt_modelo.setText("");
							txt_cor.setText("");
							txt_cilindrada.setText("");
							txt_matricula.setText("");
							txt_ano_aqui.setText("");
							txt_preco_aqui.setText("");
							txt_ano_venda.setText("");
							txt_valor_venda.setText("");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}

					}
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}


			}
		});
		btn_guardar_carro.setBounds(292, 353, 89, 23);
		inserir_carro.add(btn_guardar_carro);

		JLabel lblInserirCarro = new JLabel("Inserir Carro");
		lblInserirCarro.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblInserirCarro.setBounds(246, 11, 173, 36);
		inserir_carro.add(lblInserirCarro);

		JButton btn_sair_inserir_carro = new JButton("Sair");
		btn_sair_inserir_carro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txt_marca.setText("");
				txt_modelo.setText("");
				txt_cor.setText("");
				txt_cilindrada.setText("");
				txt_matricula.setText("");
				txt_ano_aqui.setText("");
				txt_preco_aqui.setText("");
				txt_ano_venda.setText("");
				txt_valor_venda.setText("");
				inserir_carro.setVisible(false);
				menu_inserir.setVisible(true);
			}
		});
		btn_sair_inserir_carro.setBounds(514, 373, 89, 23);
		inserir_carro.add(btn_sair_inserir_carro);
		
		JLabel label_38 = new JLabel("\u20AC");
		label_38.setBounds(411, 263, 46, 14);
		inserir_carro.add(label_38);
		
		JLabel label_39 = new JLabel("\u20AC");
		label_39.setBounds(411, 313, 46, 14);
		inserir_carro.add(label_39);



		JLabel label = new JLabel("Matr\u00EDcula: ");
		label.setBounds(156, 104, 71, 14);
		inserir_iuc.add(label);



		JLabel lblValor_1 = new JLabel("Valor: ");
		lblValor_1.setBounds(170, 144, 57, 14);
		inserir_iuc.add(lblValor_1);

		JLabel lblDataDeValidade = new JLabel("Data de Validade: ");
		lblDataDeValidade.setBounds(121, 181, 106, 14);
		inserir_iuc.add(lblDataDeValidade);

		txt_valor_iuc = new JFormattedTextField();
		adicionarMask("###", txt_valor_iuc);
		txt_valor_iuc.setBounds(237, 141, 148, 17);
		inserir_iuc.add(txt_valor_iuc);
		txt_valor_iuc.setColumns(10);

		txt_validade_iuc = new JFormattedTextField();
		adicionarMask("##/##/####", txt_validade_iuc);
		txt_validade_iuc.setColumns(10);
		txt_validade_iuc.setBounds(237, 178, 148, 17);
		inserir_iuc.add(txt_validade_iuc);

		JButton btn_guardar_iuc = new JButton("Guardar");
		btn_guardar_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					int tamanho = txt_validade_iuc.getText().trim().length();
					stmt = conn.createStatement();
					String query = "INSERT INTO IUC (Matricula, Valor, DataValidade) VALUES ('"+ combo_matricula_iuc.getSelectedItem() + 
									"' , '" + txt_valor_iuc.getText() +
									"' , '" + txt_validade_iuc.getText() + "')";					

					if( !txt_valor_iuc.getText().isEmpty() && !txt_validade_iuc.getText().isEmpty() && tamanho == 10)
						{
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
							txt_valor_iuc.setText("");
							txt_validade_iuc.setText("");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}


				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}

			}
		});
		btn_guardar_iuc.setBounds(265, 242, 89, 23);
		inserir_iuc.add(btn_guardar_iuc);

		JButton btn_sair_iuc = new JButton("Sair");
		btn_sair_iuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txt_valor_iuc.setText("");
				txt_validade_iuc.setText("");
				inserir_iuc.setVisible(false);
				menu_inserir.setVisible(true);
			}
		});
		btn_sair_iuc.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {

			}
		});
		btn_sair_iuc.setBounds(514, 373, 89, 23);
		inserir_iuc.add(btn_sair_iuc);

		JLabel lblInserirIuc = new JLabel("Inserir IUC");
		lblInserirIuc.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblInserirIuc.setBounds(248, 21, 173, 36);
		inserir_iuc.add(lblInserirIuc);
		
		JLabel label_41 = new JLabel("\u20AC");
		label_41.setBounds(386, 144, 46, 14);
		inserir_iuc.add(label_41);



		JLabel lblOficina = new JLabel("Oficina: ");
		lblOficina.setBounds(164, 65, 59, 14);
		inserir_revisao.add(lblOficina);

		JLabel lblKms = new JLabel("KMS: ");
		lblKms.setBounds(174, 90, 41, 14);
		inserir_revisao.add(lblKms);

		JLabel lblDataDeReviso = new JLabel("Data de Revis\u00E3o: ");
		lblDataDeReviso.setBounds(122, 120, 111, 14);
		inserir_revisao.add(lblDataDeReviso);

		JLabel lblObs = new JLabel("Observa\u00E7\u00F5es: ");
		lblObs.setBounds(134, 145, 99, 14);
		inserir_revisao.add(lblObs);

		JLabel lblMatricula_1 = new JLabel("Matricula: ");
		lblMatricula_1.setBounds(157, 284, 76, 14);
		inserir_revisao.add(lblMatricula_1);

		JLabel lblValor_2 = new JLabel("Valor: ");
		lblValor_2.setBounds(174, 312, 41, 14);
		inserir_revisao.add(lblValor_2);

		txt_oficina_rev = new JTextField();
		txt_oficina_rev.setColumns(10);
		txt_oficina_rev.setBounds(233, 62, 148, 17);
		inserir_revisao.add(txt_oficina_rev);

		txt_kms_rev = new JFormattedTextField();
		txt_kms_rev.setColumns(10);
		txt_kms_rev.setBounds(233, 88, 148, 17);
		inserir_revisao.add(txt_kms_rev);

		txt_data_rev = new JFormattedTextField();
		adicionarMask("##/##/####", txt_data_rev);
		txt_data_rev.setColumns(10);
		txt_data_rev.setBounds(233, 117, 148, 17);
		inserir_revisao.add(txt_data_rev);

		txt_valor_rev = new JFormattedTextField();
		adicionarMask("###", txt_valor_rev);
		txt_valor_rev.setColumns(10);
		txt_valor_rev.setBounds(233, 310, 148, 17);
		inserir_revisao.add(txt_valor_rev);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(233, 145, 148, 126);
		inserir_revisao.add(scrollPane_5);

		JTextArea textArea_obs_rev = new JTextArea();
		scrollPane_5.setViewportView(textArea_obs_rev);



		JButton btn_guardar_rev = new JButton("Guardar");
		btn_guardar_rev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {

					stmt = conn.createStatement();
					int tamanho = txt_data_rev.getText().trim().length();

					String query = "INSERT INTO Revisao (Oficina, Kms, DataRev, Obs, Matricula, Valor) VALUES ('"+ txt_oficina_rev.getText() + 
									"' , '" + txt_kms_rev.getText() +
									"' , '" + txt_data_rev.getText() + 
									"' , '" + textArea_obs_rev.getText() + 
									"' , '" + combo_matricula_rev.getSelectedItem() +	
									"' , '" + txt_valor_rev.getText() +"')";
					
					if( !txt_oficina_rev.getText().isEmpty() && 
						!txt_kms_rev.getText().isEmpty() && 
						!txt_data_rev.getText().isEmpty() && 
						!textArea_obs_rev.getText().isEmpty() &&
						!txt_valor_rev.getText().isEmpty() && tamanho == 10)
					{
						stmt.executeUpdate(query);
						JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
						txt_oficina_rev.setText("");
						txt_kms_rev.setText("");
						txt_data_rev.setText("");
						textArea_obs_rev.setText("");
						txt_valor_rev.setText("");
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
					}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				

			}
		});
		btn_guardar_rev.setBounds(262, 338, 89, 23);
		inserir_revisao.add(btn_guardar_rev);

		JButton btn_sair_rev = new JButton("Sair");
		btn_sair_rev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txt_oficina_rev.setText("");
				txt_kms_rev.setText("");
				txt_data_rev.setText("");
				textArea_obs_rev.setText("");
				txt_valor_rev.setText("");
				inserir_revisao.setVisible(false);
				menu_inserir.setVisible(true);
			}
		});
		btn_sair_rev.setBounds(514, 373, 89, 23);
		inserir_revisao.add(btn_sair_rev);

		JLabel lblInserirReviso = new JLabel("Inserir Revis\u00E3o");
		lblInserirReviso.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblInserirReviso.setBounds(233, 11, 173, 36);
		inserir_revisao.add(lblInserirReviso);
		
		JLabel label_42 = new JLabel("\u20AC");
		label_42.setBounds(383, 312, 46, 14);
		inserir_revisao.add(label_42);



		JLabel lblInserirSeguro = new JLabel("Inserir Seguro");
		lblInserirSeguro.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblInserirSeguro.setBounds(233, 11, 173, 36);
		inserir_seguro.add(lblInserirSeguro);



		JLabel label_1 = new JLabel("Matr\u00EDcula: ");
		label_1.setBounds(162, 178, 71, 14);
		inserir_seguro.add(label_1);

		JLabel label_2 = new JLabel("Valor: ");
		label_2.setBounds(175, 150, 57, 14);
		inserir_seguro.add(label_2);

		txt_valor_seg = new JFormattedTextField();
		adicionarMask("###", txt_valor_seg);
		txt_valor_seg.setColumns(10);
		txt_valor_seg.setBounds(242, 147, 148, 17);
		inserir_seguro.add(txt_valor_seg);

		txt_validade_seg = new JFormattedTextField();
		adicionarMask("##/##/####", txt_validade_seg);
		txt_validade_seg.setColumns(10);
		txt_validade_seg.setBounds(243, 119, 148, 17);
		inserir_seguro.add(txt_validade_seg);

		JLabel label_3 = new JLabel("Data de Validade: ");
		label_3.setBounds(127, 122, 106, 14);
		inserir_seguro.add(label_3);

		JButton btn_guardar_seg = new JButton("Guardar");
		btn_guardar_seg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {

					stmt = conn.createStatement();
					int tamanho = txt_validade_seg.getText().trim().length();
					String query = "INSERT INTO Seguro (Nome, DataValidade, Valor, Matricula) VALUES ('"+ txt_nome_seg.getText() + 
									"' , '" + txt_validade_seg.getText() +
									"' , '" + txt_valor_seg.getText() + 	
									"' , '" + combo_matricula_seg.getSelectedItem() +"')";

					if( !txt_nome_seg.getText().isEmpty() && 
							!txt_validade_seg.getText().isEmpty() && 
							!txt_valor_seg.getText().isEmpty() && 
							tamanho == 10)
						{
							JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
							stmt.executeUpdate(query);
							txt_nome_seg.setText("");
							txt_validade_seg.setText("");
							txt_valor_seg.setText("");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Caixas Vazias ou dados inválidos", "Dados inválidos", JOptionPane.INFORMATION_MESSAGE);
						}

				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
				
			}

		});
		btn_guardar_seg.setBounds(271, 206, 89, 23);
		inserir_seguro.add(btn_guardar_seg);

		JLabel lblNome = new JLabel("Nome: ");
		lblNome.setBounds(176, 96, 57, 14);
		inserir_seguro.add(lblNome);

		txt_nome_seg = new JTextField();
		txt_nome_seg.setColumns(10);
		txt_nome_seg.setBounds(243, 93, 148, 17);
		inserir_seguro.add(txt_nome_seg);

		JButton btn_sair_seg = new JButton("Sair");
		btn_sair_seg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txt_nome_seg.setText("");
				txt_validade_seg.setText("");
				txt_valor_seg.setText("");
				inserir_seguro.setVisible(false);
				menu_inserir.setVisible(true);

			}
		});
		btn_sair_seg.setBounds(514, 373, 89, 23);
		inserir_seguro.add(btn_sair_seg);
		
		JLabel label_43 = new JLabel("\u20AC");
		label_43.setBounds(393, 150, 46, 14);
		inserir_seguro.add(label_43);
		
		
		
		
		
		JLabel label_4 = new JLabel("Marca: ");
		label_4.setBounds(143, 84, 84, 14);
		alterar_carro.add(label_4);
		
		JLabel label_5 = new JLabel("Modelo: ");
		label_5.setBounds(143, 109, 84, 14);
		alterar_carro.add(label_5);
		
		JLabel label_6 = new JLabel("Cor: ");
		label_6.setBounds(143, 134, 84, 14);
		alterar_carro.add(label_6);
		
		JLabel label_7 = new JLabel("Cilindrada: ");
		label_7.setBounds(143, 159, 84, 14);
		alterar_carro.add(label_7);
		
		JLabel label_8 = new JLabel("Matricula: ");
		label_8.setBounds(143, 188, 84, 14);
		alterar_carro.add(label_8);
		
		JLabel label_9 = new JLabel("Combust\u00EDvel: ");
		label_9.setBounds(143, 213, 98, 14);
		alterar_carro.add(label_9);
		
		JLabel label_10 = new JLabel("Ano Aquisi\u00E7\u00E3o: ");
		label_10.setBounds(143, 238, 98, 14);
		alterar_carro.add(label_10);
		
		JLabel label_11 = new JLabel("Pre\u00E7o Aquisi\u00E7\u00E3o: ");
		label_11.setBounds(143, 263, 119, 14);
		alterar_carro.add(label_11);
		
		JLabel label_12 = new JLabel("Ano Venda: ");
		label_12.setBounds(143, 288, 98, 14);
		alterar_carro.add(label_12);
		
		JLabel label_13 = new JLabel("Valor Venda: ");
		label_13.setBounds(143, 313, 98, 14);
		alterar_carro.add(label_13);
		
		
		
		JLabel lblAlterarCarro = new JLabel("Alterar Carro");
		lblAlterarCarro.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAlterarCarro.setBounds(246, 11, 173, 36);
		alterar_carro.add(lblAlterarCarro);
		
		JButton btn_alt_carro_sair = new JButton("Sair");
		btn_alt_carro_sair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				alterar_carro.setVisible(false);
				menu_alterar.setVisible(true);
			}
		});
		btn_alt_carro_sair.setBounds(514, 373, 89, 23);
		alterar_carro.add(btn_alt_carro_sair);
		
		JLabel label_19 = new JLabel("\u20AC");
		label_19.setBounds(414, 313, 46, 14);
		alterar_carro.add(label_19);
		
		JLabel label_33 = new JLabel("\u20AC");
		label_33.setBounds(414, 263, 46, 14);
		alterar_carro.add(label_33);
		
		
		
		





	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}
	public JTextField getTxt_user() {
		return txt_user;
	}
	public JButton getBtn_login() {
		return btn_login;
	}
	public JButton getBtn_sair() {
		return btn_sair;
	}
}
