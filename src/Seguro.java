
public class Seguro {
	private String DataValidade;
	private int IdSeguro;
	private String Matricula;
	private String NomeSeguro;
	private double Valor;
	/**
	 * @param dataValidade
	 * @param idSeguro
	 * @param matricula
	 * @param nomeSeguro
	 * @param valor
	 */
	public Seguro(String dataValidade, int idSeguro, String matricula, String nomeSeguro, double valor) {
		super();
		DataValidade = dataValidade;
		IdSeguro = idSeguro;
		Matricula = matricula;
		NomeSeguro = nomeSeguro;
		Valor = valor;
	}
	/**
	 * @return the dataValidade
	 */
	public String getDataValidade() {
		return DataValidade;
	}
	/**
	 * @param dataValidade the dataValidade to set
	 */
	public void setDataValidade(String dataValidade) {
		DataValidade = dataValidade;
	}
	/**
	 * @return the idSeguro
	 */
	public int getIdSeguro() {
		return IdSeguro;
	}
	/**
	 * @param idSeguro the idSeguro to set
	 */
	public void setIdSeguro(int idSeguro) {
		IdSeguro = idSeguro;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return Matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	/**
	 * @return the nomeSeguro
	 */
	public String getNomeSeguro() {
		return NomeSeguro;
	}
	/**
	 * @param nomeSeguro the nomeSeguro to set
	 */
	public void setNomeSeguro(String nomeSeguro) {
		NomeSeguro = nomeSeguro;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return Valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		Valor = valor;
	}
	
	

}
