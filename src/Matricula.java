
public class Matricula {
	private String Matricula;

	/**
	 * @param matricula
	 */
	public Matricula(String matricula) {
		super();
		Matricula = matricula;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return Matricula;
	}


	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	
	public String toString() {
		  return Matricula;
		 }

}