
public class Revisao {
	private String DataRev;
	private int IdRevisao;
	private double Kms;
	private String Matricula;
	private String Obs;
	private String Oficina;
	private double Valor;
	/**
	 * @param dataRev
	 * @param idRevisao
	 * @param kms
	 * @param matricula
	 * @param obs
	 * @param oficina
	 * @param valor
	 */
	public Revisao(String dataRev, int idRevisao, double kms, String matricula, String obs, String oficina,
			double valor) {
		super();
		DataRev = dataRev;
		IdRevisao = idRevisao;
		Kms = kms;
		Matricula = matricula;
		Obs = obs;
		Oficina = oficina;
		Valor = valor;
	}
	/**
	 * @return the dataRev
	 */
	public String getDataRev() {
		return DataRev;
	}
	/**
	 * @param dataRev the dataRev to set
	 */
	public void setDataRev(String dataRev) {
		DataRev = dataRev;
	}
	/**
	 * @return the idRevisao
	 */
	public int getIdRevisao() {
		return IdRevisao;
	}
	/**
	 * @param idRevisao the idRevisao to set
	 */
	public void setIdRevisao(int idRevisao) {
		IdRevisao = idRevisao;
	}
	/**
	 * @return the kms
	 */
	public double getKms() {
		return Kms;
	}
	/**
	 * @param kms the kms to set
	 */
	public void setKms(double kms) {
		Kms = kms;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return Matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	/**
	 * @return the obs
	 */
	public String getObs() {
		return Obs;
	}
	/**
	 * @param obs the obs to set
	 */
	public void setObs(String obs) {
		Obs = obs;
	}
	/**
	 * @return the oficina
	 */
	public String getOficina() {
		return Oficina;
	}
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(String oficina) {
		Oficina = oficina;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return Valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		Valor = valor;
	}
	
	

}
